﻿using MultiService.Entities;
using MultiService.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MultiService.Web.Controllers
{
    public class CategoryController : Controller
    {
        CategoryOperations operation = new CategoryOperations();
        // GET: Category
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Categories()
        {
            var categories =operation.GetCategories();
            return View(categories);
        }
        [HttpPost]
        public ActionResult Create(Category category)
        {
            CategoryOperations operation = new CategoryOperations();
            operation.AddCategory(category);
            return RedirectToAction("Categories");
        }
        [HttpGet]
        public ActionResult Edit(int ID)
        {
            var category = operation.GetCategory(ID);
            return View(category);
        }
        [HttpPost]
        public ActionResult Edit(Category category)
        {
            operation.UpdateCategory(category);
            return RedirectToAction("Categories");
        }
    }
}