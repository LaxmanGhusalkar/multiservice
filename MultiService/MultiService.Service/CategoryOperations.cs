﻿using MultiService.Database;
using MultiService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiService.Service
{
    public class CategoryOperations
    {
        public void AddCategory(Category category)
        {
            using (var context = new MSContext())
            {
                context.Categories.Add(category);
                context.SaveChanges();
            }
        }
        public void UpdateCategory(Category category)
        {
            using (var context = new MSContext())
            {
                context.Entry(category).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }
        public List<Category> GetCategories()
        {
            using (var context = new MSContext())
            {
                return context.Categories.ToList(); 
            }
        }
        public Category GetCategory(int Id)
        {
            using (var context = new MSContext())
            {
                return context.Categories.Find(Id);
            }
        }
    }
}
