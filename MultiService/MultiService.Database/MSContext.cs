﻿using MultiService.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiService.Database
{
    public class MSContext:DbContext ,IDisposable
    {
        public MSContext() : base("MultiServiceDbConnection") {
        }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
