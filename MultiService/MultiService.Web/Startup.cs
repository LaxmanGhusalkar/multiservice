﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MultiService.Web.Startup))]
namespace MultiService.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
